package jms;

public class OrderClient {
	 
        public static void main(String[] args) throws Exception {
            // input arguments
            String msg = "Booking" ;
            String queueName = "jms/mdw-queue-all" ;
 
            // create the producer object and send the message
            Producer producer = new Producer();
            producer.send(queueName, msg);
        }
 
}