package jms;

public class NewTripProcessor {
	 
    public static void main(String[] args) throws Exception {
        // input arguments
        String queueName = "jms/mdw-queue-trips" ;
 
        // create the producer object and receive the message
        Consumer consumer = new Consumer();
        consumer.receive(queueName);
    }
}