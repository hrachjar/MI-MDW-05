package jms;

public class BookingsProcessor {
	 
    public static void main(String[] args) throws Exception {
        // input arguments
        String queueName = "jms/mdw-queue-bookings" ;
 
        // create the producer object and receive the message
        Consumer consumer = new Consumer();
        consumer.receive(queueName);
    }
}